//imports
const express = require('express')
const http = require('http')
const { mongoConnect } = require("./config")
const userViewModel = require('./models/user')
const { v4 } = require('uuid')

let app = express();
const server = http.createServer(app);


app.use(express.json());
app.use(
    express.urlencoded({
        extended: false,
    })
);

mongoConnect();


app.post("/createView", async (req, res) => {
    const data = new userViewModel({
        UserId: v4(),
        ViewDate: new Date(new Date().toDateString()).toISOString(),
        ProductId: req.productId
    })

    const savedData = await data.save();
    if (savedData._id) {
        res.send('success')
    }
    else {
        res.send('not able to create the data')
    }
});

app.get('/getData', async (req, res) => {
    console.log(req)
    const { customDate } = req.query
    console.log('customDate', customDate)
    var currentDate = new Date();
    var newCurrentDate = new Date()
    currentDate.setDate(currentDate.getDate() - 7);
    newCurrentDate.setDate(newCurrentDate.getDate() - 30);

    const count = await userViewModel.find().count()
    const daily = await userViewModel.find({ ViewDate: new Date(new Date().toDateString()).toISOString() }).lean();

    const days7 = await userViewModel.find({ ViewDate: { "$gte": new Date(currentDate.toDateString()).toISOString(), "$lte": new Date(new Date().toDateString()).toISOString() } }).lean()

    const days30 = await userViewModel.find({ ViewDate: { "$gte": new Date(newCurrentDate.toDateString()).toISOString(), "$lte": new Date(new Date().toDateString()).toISOString() } }).lean()

    const customData = await userViewModel.find({ ViewDate: new Date(new Date(customDate).toDateString()).toISOString() }).lean()
    res.send({
        count,
        daily,
        days7,
        days30,
        customData
    })
})

//server
server.listen(3000, () => console.log(`server running on port ${3000}`));
