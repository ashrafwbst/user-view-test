//imports
const mongoose = require("mongoose")

const Schema = mongoose.Schema;



//creating mongo database schema
const userViewSchema = new Schema({
    UserId: {
        type: String,
    },
    ViewDate: {
        type: String
    },
    ProductId: {
        type: String
    }
});

const userViewModel = mongoose.model("userViews", userViewSchema);

module.exports = userViewModel
