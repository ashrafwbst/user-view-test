const mongoose = require("mongoose")

const mongoConnect = async () => {
    try {
        mongoose.set('useCreateIndex', true);
        await mongoose.connect("mongodb://localhost:27017"
            , {
                // useNewUrlParser: true,
                useUnifiedTopology: true
            }
        );
        console.log('Connected to Mongo database')
    }
    catch (e) {
        console.log(`Error connecting to mongo database ${e}`)
    }
}

module.exports = { mongoConnect }